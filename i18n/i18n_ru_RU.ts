<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="2.0" language="ru_RU" sourcelanguage="">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../src/modules/ui/mainWindow.py" line="265"/>
        <source>Interval #2</source>
        <translation>Интервал 2</translation>
    </message>
    <message>
        <location filename="../src/modules/ui/mainWindow.py" line="266"/>
        <source>Interval #4</source>
        <translation>Интервал 4</translation>
    </message>
    <message>
        <location filename="../src/modules/ui/mainWindow.py" line="267"/>
        <source>Interval #5</source>
        <translation>Интервал 5</translation>
    </message>
    <message>
        <location filename="../src/modules/ui/mainWindow.py" line="268"/>
        <source>Interval #6</source>
        <translation>Интервал 6</translation>
    </message>
    <message>
        <location filename="../src/modules/ui/mainWindow.py" line="269"/>
        <source>Interval #1</source>
        <translation>Интервал 1</translation>
    </message>
    <message>
        <location filename="../src/modules/ui/mainWindow.py" line="270"/>
        <source>Choose source file</source>
        <translation>Выберите исходный файл</translation>
    </message>
    <message>
        <location filename="../src/modules/ui/mainWindow.py" line="271"/>
        <source>Interval #7</source>
        <translation>Интервал 7</translation>
    </message>
    <message>
        <location filename="../src/modules/ui/mainWindow.py" line="272"/>
        <source>Interval #8</source>
        <translation>Интервал 8</translation>
    </message>
    <message>
        <location filename="../src/modules/ui/mainWindow.py" line="273"/>
        <source>Interval #10</source>
        <translation>Интервал 10</translation>
    </message>
    <message>
        <location filename="../src/modules/ui/mainWindow.py" line="274"/>
        <source>Interval #9</source>
        <translation>Интервал 9</translation>
    </message>
    <message>
        <location filename="../src/modules/ui/mainWindow.py" line="275"/>
        <source>Enter numbers of columns (from ... to)</source>
        <translation>Введите номера колонок (с ... по ...)</translation>
    </message>
    <message>
        <location filename="../src/modules/ui/mainWindow.py" line="276"/>
        <source>Only unique</source>
        <translation>Режим отбора только уникальных ячеек</translation>
    </message>
    <message>
        <location filename="../src/modules/ui/mainWindow.py" line="277"/>
        <source>File name</source>
        <translation>(путь до файла)</translation>
    </message>
    <message>
        <location filename="../src/modules/ui/mainWindow.py" line="278"/>
        <source>Number of files to generate</source>
        <translation>Количество генерирумых файлов</translation>
    </message>
    <message>
        <location filename="../src/modules/ui/mainWindow.py" line="279"/>
        <source>Name for generated files</source>
        <translation>Имя генерируемых файлов</translation>
    </message>
    <message>
        <location filename="../src/modules/ui/mainWindow.py" line="280"/>
        <source>Chose destination folder</source>
        <translation>Выберите папку назначения</translation>
    </message>
    <message>
        <location filename="../src/modules/ui/mainWindow.py" line="281"/>
        <source>Folder name</source>
        <translation>(путь к папке)</translation>
    </message>
    <message>
        <location filename="../src/modules/ui/mainWindow.py" line="282"/>
        <source>Execute</source>
        <translation>Выполнить</translation>
    </message>
    <message>
        <location filename="../src/modules/ui/mainWindow.py" line="283"/>
        <source>Ctrl+Return</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/modules/ui/mainWindow.py" line="284"/>
        <source>File</source>
        <translation>Файл</translation>
    </message>
    <message>
        <location filename="../src/modules/ui/mainWindow.py" line="285"/>
        <source>Open</source>
        <translation>Открыть</translation>
    </message>
    <message>
        <location filename="../src/modules/ui/mainWindow.py" line="286"/>
        <source>Ctrl+O</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/modules/ui/mainWindow.py" line="287"/>
        <source>Reset</source>
        <translation>Сбросить</translation>
    </message>
    <message>
        <location filename="../src/modules/ui/mainWindow.py" line="288"/>
        <source>Ctrl+R</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/modules/ui/mainWindow.py" line="289"/>
        <source>Save</source>
        <translation>Сохранить</translation>
    </message>
    <message>
        <location filename="../src/modules/ui/mainWindow.py" line="290"/>
        <source>Ctrl+S</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/modules/ui/mainWindow.py" line="291"/>
        <source>Save as...</source>
        <translation>Сохранить как ...</translation>
    </message>
    <message>
        <location filename="../src/modules/ui/mainWindow.py" line="292"/>
        <source>Ctrl+Shift+S</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/modules/ui/mainWindow.py" line="264"/>
        <source>Interval #3</source>
        <translation>Интервал 3</translation>
    </message>
</context>
</TS>
