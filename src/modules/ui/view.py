from PyQt5 import QtWidgets
from modules.ui.mainWindow import Ui_MainWindow
from modules.ui.handlers import setupHandlers
from modules.combiner import Combiner
from modules.excel import Excel
from modules.results import Results

class ViewApp(QtWidgets.QMainWindow, Ui_MainWindow):
    def __init__(self, Settings):
        super().__init__()
        self.setupUi(self)
        self.settings = Settings()
        self.applySettings(True)
        self.initHandlers()
    
    def initHandlers(self):
        setupHandlers(self)

    def applySettings(self, isDefault = False):
        if isDefault:
            self.settings.load()
        self.labelSrcFileName.setText(self.settings.settings["sourceFilePath"])
        self.labelDestFolderName.setText(self.settings.settings["destFolderPath"])
        self.spinBoxColsRangeFrom.setValue(self.settings.settings["columnsStart"])
        self.spinBoxColsRangeTo.setValue(self.settings.settings["columnsEnd"])
        self.switchRowRangesActivity(self.spinBoxColsRangeTo.value() - self.spinBoxColsRangeFrom.value() + 1)

        i = 1
        fieldFromPrefix = 'spinBoxRowsRangeFrom_'
        fieldToPrefix = 'spinBoxRowsRangeTo_'
        while i < 11:
            saved = self.settings.settings["rowsRanges"][i-1]
            (start, end) = saved.split(':')
            ii = str(i)
            getattr(self, fieldFromPrefix + ii).setValue(int(start))
            getattr(self, fieldToPrefix + ii).setValue(int(end))
            i+=1

        self.checkBoxOnlyUniq.setChecked(bool(self.settings.settings["randomizerMode"]))
        self.spinBoxFilesNumber.setValue(self.settings.settings["numberOfFiles"])
        self.lineEditFilesNamePrefix.setText(self.settings.settings["destFileNamePrefix"])

    def refreshSettings(self, fieldNameFromView = ''):
        if fieldNameFromView == 'labelSrcFileName':
            self.settings.settings["sourceFilePath"] = self.labelSrcFileName.text()
        elif fieldNameFromView == 'labelDestFolderName':
            self.settings.settings["destFolderPath"] = self.labelDestFolderName.text()
        elif fieldNameFromView == 'spinBoxColsRangeFrom':
            self.settings.settings["columnsStart"] = self.spinBoxColsRangeFrom.value()
        elif fieldNameFromView == 'spinBoxColsRangeTo':
            self.settings.settings["columnsEnd"] = self.spinBoxColsRangeTo.value()
        elif fieldNameFromView == 'checkBoxOnlyUniq':
            self.settings.settings["randomizerMode"] = int(self.checkBoxOnlyUniq.isChecked())
        elif fieldNameFromView == 'spinBoxFilesNumber':
            self.settings.settings["numberOfFiles"] = self.spinBoxFilesNumber.value()
        elif fieldNameFromView == 'lineEditFilesNamePrefix':
            self.settings.settings["destFileNamePrefix"] = self.lineEditFilesNamePrefix.text()
        else:
            (name, index) = fieldNameFromView.split('_')
            if index and (name == 'spinBoxRowsRangeFrom' or name == 'spinBoxRowsRangeTo'):
                fieldFromPrefix = 'spinBoxRowsRangeFrom_'
                fieldToPrefix = 'spinBoxRowsRangeTo_'
                start = str(getattr(self, fieldFromPrefix + index).value())
                end = str(getattr(self, fieldToPrefix + index).value())
                val = start + ':' + end
                self.settings.settings["rowsRanges"][int(index) - 1] = val
    
    def saveSettingsAs(self, path):
        self.settings.saveAs(path)

    def switchRowRangesActivity(self, numberOfColumns = 1):
        labelPrefix = 'labelRowsRange_'
        fieldFromPrefix = 'spinBoxRowsRangeFrom_'
        fieldToPrefix = 'spinBoxRowsRangeTo_'
        i = 1
        while i < 11:
            ii = str(i)
            if i <= numberOfColumns:
                getattr(self, labelPrefix + ii).setEnabled(True)
                getattr(self, fieldFromPrefix + ii).setEnabled(True)
                getattr(self, fieldToPrefix + ii).setEnabled(True)
            else:
                getattr(self, labelPrefix + ii).setEnabled(False)
                getattr(self, fieldFromPrefix + ii).setEnabled(False)
                getattr(self, fieldToPrefix + ii).setEnabled(False)
            i+=1

    def execute(self):
        self.progressBar.reset()
        self.pushButtonExecute.setEnabled(False)
        combiner = Combiner()
        excel = Excel()
        results = Results()
        self.addRangesToCombiner(combiner)
        excel.setSrcFileName(self.settings.settings["sourceFilePath"])
        excel.setColNums((self.spinBoxColsRangeFrom.value(), self.spinBoxColsRangeTo.value()))
        excel.setWorkSheet()
        results.setFileNamePrefix(self.lineEditFilesNamePrefix.text())
        results.setDestFolder(self.labelDestFolderName.text())
        results.detectLastIndexByPrefixInfolder()
        

        # in loop
        numOfFiles = self.spinBoxFilesNumber.value()
        self.progressBar.setMaximum(numOfFiles)
        i = 0
        while i < numOfFiles:
            listCells = combiner.getRandomCellsFromRanges()
            excel.setRowsCells(listCells)
            text = excel.getResultText()
            results.setResult(text)
            results.writeFile()
            results.lastIndex+=1
            i+=1
            self.progressBar.setValue(i)
        
        self.pushButtonExecute.setEnabled(True)
        self.statusBar().showMessage('Complete')


    def addRangesToCombiner(self, combiner):
        isEnabled = True
        i = 1
        labelPrefix = 'labelRowsRange_'
        fieldFromPrefix = 'spinBoxRowsRangeFrom_'
        fieldToPrefix = 'spinBoxRowsRangeTo_'
        while i < 11 and isEnabled == True:
            ii = str(i)
            isEnabled = getattr(self, labelPrefix + ii).isEnabled()
            if isEnabled:
                combiner.ranges.append((getattr(self, fieldFromPrefix + ii).value(), getattr(self, fieldToPrefix + ii).value()))
            i+=1



