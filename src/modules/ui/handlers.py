import os  # Отсюда нам понадобятся методы для отображения содержимого директорий
from PyQt5 import QtWidgets

def setupHandlers(View):
    View.pushButtonSrc.clicked.connect(lambda: onClickPushBtnSrcFile(View))
    View.spinBoxColsRangeFrom.valueChanged.connect(lambda: onColsRangeChanged(View, 'spinBoxColsRangeFrom'))
    View.spinBoxColsRangeTo.valueChanged.connect(lambda: onColsRangeChanged(View, 'spinBoxColsRangeTo'))
    
    setupRowsRangeHandlers(View)

    View.pushButtonDestFolder.clicked.connect(lambda: onClickPushBtnDestFolder(View))
    View.pushButtonExecute.clicked.connect(lambda: onClickPushBtnExecute(View))
    View.checkBoxOnlyUniq.stateChanged.connect(lambda: onCheckOnlyUniq(View))
    View.spinBoxFilesNumber.valueChanged.connect(lambda: onFilesNumberChanged(View))
    View.lineEditFilesNamePrefix.textChanged.connect(lambda: onFilesNamePrefixChanged(View))

    # Menu bar
    View.actionOpen.triggered.connect(lambda: onActionOpen(View))
    View.actionReset.triggered.connect(lambda: onActionReset(View))
    View.actionSave.triggered.connect(lambda: onActionSave(View))
    View.actionSave_as.triggered.connect(lambda: onActionSave_as(View))

def onClickPushBtnSrcFile(View):
    browseSrcFile(View)

def onClickPushBtnDestFolder(View):
    browseDestFolder(View)

def browseSrcFile(View):
    options = QtWidgets.QFileDialog.Options()
    file = QtWidgets.QFileDialog.getOpenFileName(View, "Выберите file", './files/', "Excel (*.xlsx *.xls)", options = options)

    if file:
        View.labelSrcFileName.setText(file[0])
        View.refreshSettings('labelSrcFileName')

def browseDestFolder(View):
    options = QtWidgets.QFileDialog.Options()
    folder = QtWidgets.QFileDialog.getExistingDirectory(View, "Choose dest folder", './files/', options = options)
    
    if folder:
        View.labelDestFolderName.setText(folder)
        View.refreshSettings('labelDestFolderName')

def onColsRangeChanged(View, fieldName):
    fromNumber = View.spinBoxColsRangeFrom.value()
    toNumber = View.spinBoxColsRangeTo.value()
    
    if(fromNumber >= toNumber):
        toNumber = fromNumber + 1
        View.spinBoxColsRangeTo.setValue(toNumber)
    
    columnCount = toNumber - fromNumber + 1
    if columnCount > 10:
        columnCount = 10
        toNumber = fromNumber + 9
        View.spinBoxColsRangeTo.setValue(toNumber)
    View.switchRowRangesActivity(columnCount)
    
    View.refreshSettings(fieldName)
    
def setupRowsRangeHandlers(View):
    # fieldFromPrefix = 'spinBoxRowsRangeFrom_'
    # fieldToPrefix = 'spinBoxRowsRangeTo_'
    # i = 1
    # while i < 11:
    #     ii = str(i)
    #     getattr(View, fieldFromPrefix + ii).valueChanged.connect(lambda: onRowsRangeChanged(View, fieldFromPrefix + ii))
    #     getattr(View, fieldToPrefix + ii).valueChanged.connect(lambda: onRowsRangeChanged(View, fieldToPrefix + ii))
    #     i+=1
    # ---
    # this code up here doesn't work, I don't know why now

    View.spinBoxRowsRangeFrom_1.valueChanged.connect(lambda: onRowsRangeChanged(View, 'spinBoxRowsRangeFrom_1'))
    View.spinBoxRowsRangeTo_1.valueChanged.connect(lambda: onRowsRangeChanged(View, 'spinBoxRowsRangeTo_1'))
    View.spinBoxRowsRangeFrom_2.valueChanged.connect(lambda: onRowsRangeChanged(View, 'spinBoxRowsRangeFrom_2'))
    View.spinBoxRowsRangeTo_2.valueChanged.connect(lambda: onRowsRangeChanged(View, 'spinBoxRowsRangeTo_2'))
    View.spinBoxRowsRangeFrom_3.valueChanged.connect(lambda: onRowsRangeChanged(View, 'spinBoxRowsRangeFrom_3'))
    View.spinBoxRowsRangeTo_3.valueChanged.connect(lambda: onRowsRangeChanged(View, 'spinBoxRowsRangeTo_3'))
    View.spinBoxRowsRangeFrom_4.valueChanged.connect(lambda: onRowsRangeChanged(View, 'spinBoxRowsRangeFrom_4'))
    View.spinBoxRowsRangeTo_4.valueChanged.connect(lambda: onRowsRangeChanged(View, 'spinBoxRowsRangeTo_4'))
    View.spinBoxRowsRangeFrom_5.valueChanged.connect(lambda: onRowsRangeChanged(View, 'spinBoxRowsRangeFrom_5'))
    View.spinBoxRowsRangeTo_5.valueChanged.connect(lambda: onRowsRangeChanged(View, 'spinBoxRowsRangeTo_5'))
    View.spinBoxRowsRangeFrom_6.valueChanged.connect(lambda: onRowsRangeChanged(View, 'spinBoxRowsRangeFrom_6'))
    View.spinBoxRowsRangeTo_6.valueChanged.connect(lambda: onRowsRangeChanged(View, 'spinBoxRowsRangeTo_6'))
    View.spinBoxRowsRangeFrom_7.valueChanged.connect(lambda: onRowsRangeChanged(View, 'spinBoxRowsRangeFrom_7'))
    View.spinBoxRowsRangeTo_7.valueChanged.connect(lambda: onRowsRangeChanged(View, 'spinBoxRowsRangeTo_7'))
    View.spinBoxRowsRangeFrom_8.valueChanged.connect(lambda: onRowsRangeChanged(View, 'spinBoxRowsRangeFrom_8'))
    View.spinBoxRowsRangeTo_8.valueChanged.connect(lambda: onRowsRangeChanged(View, 'spinBoxRowsRangeTo_8'))
    View.spinBoxRowsRangeFrom_9.valueChanged.connect(lambda: onRowsRangeChanged(View, 'spinBoxRowsRangeFrom_9'))
    View.spinBoxRowsRangeTo_9.valueChanged.connect(lambda: onRowsRangeChanged(View, 'spinBoxRowsRangeTo_9'))
    View.spinBoxRowsRangeFrom_10.valueChanged.connect(lambda: onRowsRangeChanged(View, 'spinBoxRowsRangeFrom_10'))
    View.spinBoxRowsRangeTo_10.valueChanged.connect(lambda: onRowsRangeChanged(View, 'spinBoxRowsRangeTo_10'))

def onRowsRangeChanged(View, fieldName):
    View.refreshSettings(fieldName)

def onCheckOnlyUniq(View):
    View.refreshSettings('checkBoxOnlyUniq')

def onFilesNumberChanged(View):
    View.refreshSettings('spinBoxFilesNumber')

def onFilesNamePrefixChanged(View):
    View.refreshSettings('lineEditFilesNamePrefix')

def onClickPushBtnExecute(View):
    View.statusBar().showMessage('Program runs')
    View.execute()

def onActionOpen(View):
    options = QtWidgets.QFileDialog.Options()
    View.statusBar().showMessage('Action: open saved settings')

    file = QtWidgets.QFileDialog.getOpenFileName(View, "Выберите", './settings/', "Settings (*.json)", options = options)

    if file:
        View.settings.loadByPath(file[0])
        View.applySettings()

def onActionReset(View):
    View.statusBar().showMessage('Action: reset settings to default')
    View.applySettings(True)

def onActionSave(View):
    options = QtWidgets.QFileDialog.Options()
    if View.settings.currentFilePath:
        View.settings.save()
    else:
        View.statusBar().showMessage('Action: save current settings')
        file = QtWidgets.QFileDialog.getSaveFileName(View, "Выберите", './settings/', "Settings (*.json)", options = options)
        if file:
            View.saveSettingsAs(file[0])

def onActionSave_as(View):
    options = QtWidgets.QFileDialog.Options()
    View.statusBar().showMessage('Action: save settings as new or existing file')
    file = QtWidgets.QFileDialog.getSaveFileName(View, "Выберите", './settings/', "Settings (*.json)", options = options)
    if file:
        View.saveSettingsAs(file[0])
