# Open file
# Get Data
# import random
# from openpyxl import load_workbook
# wb2 = load_workbook('files/1.xlsx')
# ws = wb2.active

# j = 0
# sep = ' '
# index = 1
# while j < 200:
#     results = []
#     result = ''
#     i = 1
#     while i < 11:
#         row = random.randrange(10, 10010, 1)
#         results.append(str(ws.cell(row = row, column = i).value))
#         i+=1
#     result = sep.join(results)


#     # Filename to write
#     name = "saved"
#     filename = name + str(index) + ".txt"
    
#     # Open the file with writing permission
#     myfile = open('files/' + filename, 'w')
    
#     # Write a line to the file
#     myfile.write(result)
    
#     # Close the file
#     myfile.close()
#     index+=1
#     j+=1

from openpyxl import load_workbook

class Excel:
    def __init__(self, *args, **kwargs):
        self.srcFileName = ''
        self.colsNums = None # must be tuple (1, 10)
        self.rowsCells = None # must be list with length equals of delta colNums
        self.resultText = ''
        self.sep = ' '
        self.sheet = None

        return super().__init__(*args, **kwargs)

    def setSrcFileName(self, name):
        self.srcFileName = name

    def setColNums(self, colNumsTuple):
        self.colsNums = colNumsTuple

    def setRowsCells(self, rowCellsList):
        self.rowsCells = rowCellsList

    def setWorkSheet(self):
        wb = load_workbook(self.srcFileName)
        self.sheet = wb.active

    # def getCellValue(self, col, row):
    #     value = ''

    #     return value

    def getResultText(self):
        i = self.colsNums[0]
        j = self.colsNums[1]
        k = 0
        res = []
        while i <= j:
            row = self.rowsCells[k]
            cell = str(self.sheet.cell(row = row, column = i).value)
            res.append(cell)
            i+=1
            k+=1
        self.resultText = self.sep.join(res)
        return self.resultText