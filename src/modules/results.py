# get result-string from combiner
# get filename from ui and path
# increment of filename if needs
# save file
import os

class Results:
    def __init__(self, *args, **kwargs):
        self.result = ''
        self.fileNamePrefix = ''
        self.destFolder = './'
        self.lastIndex = 1
        return super().__init__(*args, **kwargs)

    def setResult(self, text = ''):
        self.result = text
    
    def setFileNamePrefix(self, prefix = 'file'):
        self.fileNamePrefix = prefix

    def setDestFolder(self, folder = './'):
        self.destFolder = folder

    def detectLastIndexByPrefixInfolder(self):
        path = os.path.join(self.destFolder, self.fileNamePrefix + "%s" + ".txt")
        while os.path.exists(path % self.lastIndex):
            self.lastIndex += 1

    def writeFile(self):
        path = os.path.join(self.destFolder, self.fileNamePrefix + str(self.lastIndex) + ".txt")
        file = open(path, 'w')
        file.write(self.result)
        file.close()
