# get ranges
# returns randomized list of cells
import random
class Combiner:

    def __init__(self, *args, **kwargs):
        self.ranges = []
        return super().__init__(*args, **kwargs)

    def getRandomCellsFromRanges(self):
        length = len(self.ranges)
        results = []
        i=0
        while i < length:
            cell = random.randrange(self.ranges[i][0], self.ranges[i][1], 1)
            results.append(cell)
            i+=1
        return results