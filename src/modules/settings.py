import json
import os

class Settings:
    
    # Constructor
    def __init__(self, *args, **kwargs):
        self.settings = dict.fromkeys([
            "sourceFilePath", "columnsStart", "columnsEnd", "rowsRanges", 
            "randomizerMode", "numberOfFiles", "destFileNamePrefix", "destFolderPath"
        ])
        self.load()
        self.isDefault = True
        self.currentFilePath = None
        return super().__init__(*args, **kwargs)

    def save(self):
        self.saveAs(self.currentFilePath)

    def saveAs(self, path):
        encoded = json.dumps(self.settings)
        self.isDefault = self.checkDefault(path)
        if not self.isDefault:
            with open(path, "w") as write_file:
                write_file.write(encoded)
                write_file.close()
                self.currentFilePath = path

    def load(self, name = 'default'):
        path = './settings/' + name + ".json"
        if name == 'default':
            self.isDefault = True
        else:
            self.isDefault = False
        with open(path, "r") as read_file:
            if read_file:
                self.settings = json.load(read_file)
                self.isDefault = self.checkDefault(path)
                if self.isDefault:
                    pass
                else:
                    self.currentFilePath = path
                    pass


    def checkDefault(self, path):
        drive, path_and_file = os.path.splitdrive(path)
        path, file = os.path.split(path_and_file)
        fileName, ext = file.split('.', -1)
        if fileName == 'default':
            return True
        return False



    def loadByPath(self, path):
        if len(path):
            isDefault = self.checkDefault(path)
            self.isDefault = isDefault
            with open(path, "r") as read_file:
                if read_file:
                    self.settings = json.load(read_file)
                    self.checkDefault(path)
                    if self.isDefault:
                        pass
                    else:
                        self.currentFilePath = path
                        pass
        else:
            pass

    def reset(self):
        self.load()
