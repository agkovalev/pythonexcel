import sys  # sys нужен для передачи argv в QApplication
from PyQt5 import QtWidgets
from PyQt5.QtCore import QTranslator, QLocale
from modules.settings import Settings
from modules.ui.view import ViewApp
import os

I18N_QT_PATH = str(os.path.join(os.path.abspath('.'), 'i18n'))

def main():
    app = QtWidgets.QApplication(sys.argv)  # Новый экземпляр QApplication
    
    locale = 'ru_RU'
    app_translator = QTranslator(app)
    app_translator.load('{}/{}.qm'.format(I18N_QT_PATH, 'i18n_%s' % locale))
    app.installTranslator(app_translator)

    window = ViewApp(Settings)  # Создаём объект класса ViewApp
    window.show()  # Показываем окно
    app.exec_()  # и запускаем приложение

if __name__ == '__main__':  # Если мы запускаем файл напрямую, а не импортируем
    main()  # то запускаем функцию main()